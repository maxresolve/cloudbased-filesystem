Cryptographic Access Control in a Cloud Based File Storage Environment
======================================================================

### What is this repository for? ###

The cloud is everywhere and more applications are turning into cloud based applications. The reason for this is that people need their pictures, videos and so on, available at any time and everywhere. Cloud based file storage solutions is a thing that has arrived to stay, but the data needs to be protected, while sending, receiving and storing it as it should not be available to any other than the owner(s). However, the owners and administrators of the data should be able to share it with whomever they want to. Meaning it should be available to any person they desire.
To solve the problem of cloud based file storage it is suggested to use cryptographic access control combined with proper key management, to make sure that only the correct users can get access to the data. This involves encryption of the data before it is transmitted to the remote server. This means that the data stored on the remote server is always encrypted.
In this project the cloud based file storage problem is solved using MediaWiki as the remote server storage together with proper key management. This gives an environment that is easy to setup for everyone as well as a very clean cryptography solution. All data handled is encrypted already at the client which means that authentication of the server is not necessary.
From this project it has been learned that a cloud based file storage is a really complex problem involving many sub problems. Even when creating a simple clean solution it gets complex with a lot of needed choices to be made.

The goal of this project is to create a very easy to setup cloud based file storage solution. Combining cryptographic access control together with proper key management giving a very clean cryptography setup. MediaWiki should be used for the remote storage and FUSE for the local file system. The goals/problems of the project can be described as follows:

* Implement a cloud based file storage using MediaWiki backend.
* Uploading and downloading data to and from the MediaWiki.
* Protect the data, so it is only available to the owner(s).
* Implement a way for a user to share files with another user, without a third part getting access to the same data.
* ~~Implement a client file system using FUSE.~~

### How do I get set up? ###

The lastest stable release is zipped into `target.zip`. If an unstable release is wanted it is possible to build it by yourself by creating a jar (`mvn package`) or just run the code.

Run the release from the target folder (after target.zip has been unzipped) by running the command `java -jar cloudbased-filesystem-1.0-SNAPSHOT.jar <config file>`. Remember to include the config file.
The config file can be re-created from the `config.template.xml`.

### Contribution guidelines ###

All source code has been included in `src/`. The directory structure of this project is structured by feature-folder. This means each feature has its own folder containing:

* Functionality (service, controller, repository, etc.)
* Test cases (only for this functionality)
* Optionally XML schema

### Who do I talk to? ###

This project has been developed by Mads Lundt and Kristian Flamsted and has been submitted June 4th, 2016 at Technical University of Denmark.

*"We would like to thank our supervisor Christian D. Jensen for help, advise and for giving great inputs and ideas during this project."*