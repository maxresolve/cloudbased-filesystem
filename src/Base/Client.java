package Base;

import Config.Config;
import Cryptography.Asymmetric;
import Cryptography.Symmetric;
import Key.Key;
import Key.KeyRing;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by mads on 4/21/16.
 */
public class Client {
    private Key _key = null;
    private KeyRing _keyRing;
    private File _localRootDir;
    private Config _config;
    private boolean _clientLoaded = false;

    public Client(String xmlPath) {
        this._config = Config.loadConfig(xmlPath);
        this._init();
    }

    public Client() {
        this._config = Config.loadConfig();
        this._init();
    }

    private void _init() {
        _localRootDir = new File(_config.getRootDirectory());
        File file = new File(_config.getLocalKeyPath());
        if (!file.exists() || !file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            this._key = Key.loadKeyFromXML(file);
        }
        if (this._key == null) {
            System.out.println("No key was found. Do you want us to create one for you (Y/N)? ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                String create = br.readLine();
                if (create.equalsIgnoreCase("y")) {
                    this._key = this._createKey(file);
                } else {
                    file.delete();
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Key was succesfully loaded.");
        }
        if (!_localRootDir.isDirectory()) {
            _localRootDir.mkdir();
            System.out.println("Couldn't find the defined root dir, but it has been created.");
        }
        file = this._key.download(this._localRootDir.getPath(), true);
        File newFile = new File(this._localRootDir.getPath() + "/" + this._key.getName() + ".xml");
        newFile = this._key.decrypt(file, newFile, true);
        if (newFile != null) {
            this._keyRing = KeyRing.loadXML(newFile.getPath());
            newFile.delete();
            this._clientLoaded = true;
        }
    }

    public boolean getClientLoaded() {
        return this._clientLoaded;
    }

    public void downloadFilesToDir() {
        System.out.println("\nFiles are being downloaded...");
        this._keyRing.downloadAll(this._localRootDir.getPath(), true);
        System.out.println("\nFiles have been downloaded and decrypted.");
    }

    public void end() {
        // Uploading all files and then deletes them
        for (File file : _localRootDir.listFiles()) {
            if (!file.isDirectory()) {
                file.delete();
            }
        }
    }

    public void uploadFile(File file, boolean moveFile) {
        if (!file.exists()) {
            return;
        }

        System.out.println("\nAttempting to upload file: " + file.getName());
        Key key = this._keyRing.getKeyByFilename(file.getName());

        if (key == null) {
            key = new Key(file);
            this._keyRing.addKey(key);
            this._key.encryptAndUpload(this._keyRing);
            key.encryptAndUpload(file);
        } else {
            if (key.getWriteKey() == null || key.getWriteKey().isEmpty()) {
                System.out.println("\tMissing permission. You do not have write access to the current file.\n\tFile can not be uploaded.");
                return;
            } else {
                if (key.encryptAndUpload(file)) {
                    System.out.println("\t" + file.getName() + " was uploaded with existing key.");
                }
            }
        }
        if (moveFile) {
            try {
                InputStream in = new FileInputStream(file);
                OutputStream out = new FileOutputStream(this._localRootDir.getPath() + "/" + file.getName());
                IOUtils.copy(in, out);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void uploadAllFiles() {
        // Running trough all the files in the rootdir
        for (File file : _localRootDir.listFiles()) {
            if (!file.isDirectory()) {
                uploadFile(file, false);
            }
        }
    }

    public void renameFile(String oldName, String newName) {
        File oldFile = new File(_localRootDir.getPath() + "/" + oldName);
        File newFile = new File(_localRootDir.getPath() + "/" + newName);

        if (newFile.exists()) {
            System.out.println("\t" + newName + " already exists, delete or try another name.");
            return;
        }
        Key key = this._keyRing.getKeyByFilename(oldName);
        if (key == null) {
            System.out.println("Key could not be found");
        }
        if (oldFile.renameTo(newFile)) {
            System.out.println("\t" + oldName + " was succesfully renamed to " + newName);
            key.setName(newName);
            KeyRing keyRing = this._keyRing.getKeyRingContainingKey(key.getId());
            keyRing.addKey(key, true);
            if (this._key.getReference().equals(keyRing.getId())) {
                key = this._key;
            } else {
                key = this._keyRing.getKeyReferingToKeyRing(keyRing.getId());
            }
            key.encryptAndUpload(keyRing);
        } else {
            System.out.println("File was not renamed. Try again.");
        }
    }

    // Method moving a file from one point to another
    private boolean _moveFile(String from, String to) {
        Path movefrom = FileSystems.getDefault().getPath(from);
        Path target = FileSystems.getDefault().getPath(to);
        try {
            Files.move(movefrom, target, REPLACE_EXISTING);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private Key _createKey(File file) {
        System.out.println("Creating new key.");
        KeyRing keyRing = new KeyRing(new ArrayList<Key>());
        Key key = new Key(keyRing);

        System.out.println("Key generated with empty key ring. Uploading key ring...");
        key.encryptAndUpload(keyRing);

        key.saveXML(file);
        System.out.println("Key was successfully created.");

        return key;
    }

    public boolean addKey(String path, String secret, String privateKey) {
        File encryptedFile = new File(path);
        if (!encryptedFile.exists() || !encryptedFile.isFile()) {
            System.out.println("Could not find file");
            return false;
        } else {
            Key key = Key.loadKeyFromXML(encryptedFile, privateKey, secret);
            this._keyRing.addKey(key);
            this._key.encryptAndUpload(this._keyRing);
            return true;
        }
    }

    public String exportKey(Key key, String publicKey, String path, String permission) {
        File file = new File(path);
        if (!file.exists() || !file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        boolean read = true;
        boolean write = true;
        if (permission.length() > 0) {
            permission = permission.toLowerCase();
            read = permission.indexOf("r") != -1;
            write = permission.indexOf("w") != -1;

            if (read && (key.getReadKey() == null || key.getReadKey().isEmpty())) {
                read = false;
                System.out.println("\n\tYou do not have read access. Read key can not be added");
            }
            if (write && (key.getWriteKey() == null || key.getWriteKey().isEmpty())) {
                write = false;
                System.out.println("\n\tYou do not have write access. Write key can not be added");
            }
        }
        if (!read && !write) {
            System.out.println("Can not export key with no permissions. Export aborted...");
            file.delete();
            return null;
        }
        if (write && !read) {
            System.out.println("Can not export a key with only write permission. Export aborted...");
            file.delete();
            return null;
        }
        Key newKey = key.exportKey(read, write);

        Asymmetric asym = new Asymmetric(publicKey);
        String secret = Symmetric.generateKey();
        String iv = Symmetric.generateIv();
        Symmetric sym = new Symmetric(secret, iv);
        File decryptedFile = new File(newKey.getId());
        newKey.saveXML(decryptedFile);

        try {
            InputStream in = new FileInputStream(decryptedFile);
            OutputStream out = new FileOutputStream(file);
            sym.encrypt(in, out);

            decryptedFile.delete();

            return asym.encryptByUsingPublicKey(secret + " : " + iv);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\nAn error occurred doing the encryption. Please make sure have have pasted the correct public key");
        return null;
    }

    public Key getKeyByFilename(String filename) {
        return this._keyRing.getKeyByFilename(filename);
    }
    public Key getKeyById(String id) {
        return this._keyRing.getKey(id);
    }

    public Asymmetric generateKey() {
        return new Asymmetric();
    }
}
