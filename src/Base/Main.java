package Base;

import Cryptography.Asymmetric;
import Key.Key;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static BufferedReader _br = new BufferedReader(new InputStreamReader(System.in));
    private static String[] _menu = {
            "Download all files",
            "Sync all files",
            "Rename file",
            "Add new file",
            "Add key",
            "Export key (by filename)",
            "Export key (by id)"
    };
    private static Client _client;
    public static void main(String[] args) throws IOException {
        String userInput;
        _printLine("Welcome to BoxDrop CloudFile Storage.");

        _printLine("Loading Client...");

        if (args.length > 0) {
            _client = new Client(args[0]);
        } else {
            _client = new Client();
        }

        if (!_client.getClientLoaded()) {
            _printLine("Client wasn't loaded properly. Want to try again?(Y/N)");
            userInput = _br.readLine();
            if (userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("yes")) {
                _client = new Client();
            } else if (userInput.equalsIgnoreCase("n") || userInput.equalsIgnoreCase("no")) {
                _printLine("Closing application.");
                return;
            }
            if (!_client.getClientLoaded()) {
                _printLine("Client wasn't loaded properly. Closing Application");
                return;
            }
        } else {
            _printLine("Client was loaded successfully.");
        }
        _printLine("Now downloading files to your directory...");
        _client.downloadFilesToDir();
        _printLine("All files are now stored locally.");
        while (true) {
            System.out.println();
            _printMenu();
            userInput = _br.readLine();
            int menuValue;
            try {
                menuValue = Integer.parseInt(userInput);
            } catch(NumberFormatException e) {
                System.out.printf("%s is not a valid user input", userInput);
                continue;
            }

            if (menuValue == -1) {
                _print("Do you want to upload your files on termination Y/N? ");
                while (true) {
                    userInput = _br.readLine();
                    if (userInput.equalsIgnoreCase("y")) {
                        _printLine("Uploading your files before termination.");
                        _client.uploadAllFiles();
                        break;
                    } else if (userInput.equalsIgnoreCase("n")) {
                        break;
                    } else {
                        _printLine("Wrong input. Try again [Y/N]");
                    }
                }
                break;
            } else {
                loadOption(menuValue);
            }
        }
        _client.end();
        _printLine("\nClosing application. Thank you for using BoxDrop!");
    }

    private static void _printMenu() {
        _printLine("\nYou have the following options:");
        for (int i = 0; i < _menu.length; i++) {
            System.out.printf("\t[%d]\t\t%s\n", i, _menu[i]);
        }
        _printLine("\n\t[-1]\tExit");
    }

    private static void loadOption(int index) throws IOException {
        String userInput;
        Key key;
        switch(index) {
            case 0:
                _client.downloadFilesToDir();
                break;
            case 1:
                _client.uploadAllFiles();
                break;
            case 2:
                _print("Please input filename (it's case sensitive and include file extension): ");
                userInput = _br.readLine();
                String newName;
                _print("Please input new name (include extension): ");
                newName = _br.readLine();
                _client.renameFile(userInput, newName);
                break;
            case 3:
                _print("Please input filename (it's case sensitive and include file extension): ");
                userInput = _br.readLine();
                File file = new File(userInput);
                if (file.exists() && file.isFile()) {
                    _client.uploadFile(file, true);
                } else {
                    _printLine("File does not exists or is not a file");
                }
                break;
            case 4:
                Asymmetric keys = _client.generateKey();
                _printLine("Copy and send key:\n" + keys.getPublicKey() + "\n");
                _print("Please input filename for the provided key (it's case sensitive and include file extension): ");
                userInput = _br.readLine();
                String secret;
                _print("Please input the secret: ");
                secret = _br.readLine();
                if (_client.addKey(userInput, secret, keys.getPrivateKey())) {
                    _printLine("Key is added");
                    _client.downloadFilesToDir();
                }

                break;
            case 5:
                _print("Please input filename (it's case sensitive and include file extension): ");
                userInput = _br.readLine();
                key = _client.getKeyByFilename(userInput);
                if (key == null) {
                    _print("Could not find file");
                } else {
                    _print("Please input your provided public key: ");
                    String publicKey = _br.readLine();
                    _print("Please input permissions for the key exported [R]ead and/or [W]rite (Leave blank if you want to give the same access): ");
                    String permissions = _br.readLine();
                    _print("Please input the destination for your output: ");
                    String path = _br.readLine();
                    String newSecret = _client.exportKey(key, publicKey, path, permissions);
                    if (newSecret != null) {
                        _printLine("Copy and send key:\n" + newSecret + "\n");
                    }
                }
                break;
            case 6:
                _print("Please input key id: ");
                userInput = _br.readLine();
                key = _client.getKeyById(userInput);
                if (key == null) {
                    _printLine("Could not find key");
                } else {
                    _print("Please input your provided public key: ");
                    String publicKey = _br.readLine();
                    _print("Please input permissions for the key exported [R]ead and/or [W]rite (Leave blank if you want to give the same access): ");
                    String permissions = _br.readLine();
                    _print("Please input the destination for your output: ");
                    String path = _br.readLine();
                    String newSecret = _client.exportKey(key, publicKey, path, permissions);
                    if (newSecret != null) {
                        _printLine("Copy and send key:\n" + newSecret + "\n");
                    }
                }
                break;
            default:
                System.out.printf("%s is not a valid user input", index);
        }
    }

    private static void _printLine(String string) {
        System.out.println(string);
    }
    private static void _print(String string) {
        System.out.print(string);
    }

}
