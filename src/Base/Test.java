package Base;

import Config.Config;
import info.bliki.api.Connector;
import info.bliki.api.Page;
import info.bliki.api.User;

import java.util.List;

/**
 * Created by mads on 5/7/16.
 */
public class Test {
    public static void main(String args[]) {
        Config config = Config.loadConfig();
        User user = new User(config.getMediawiki().getUsername(), config.getMediawiki().getPassword(), config.getMediawiki().getUrl() + "/api.php");
        Connector connector = new Connector();
        user = connector.login(user);

        System.out.println(user.login());

        List<Page> images = user.queryImageinfo(new String[] { "File:17d92086-4c10-4ac8-9ef7-60dbe3d946d6.box" });
        List<Page> contents = user.queryContent(new String[] { "File:17d92086-4c10-4ac8-9ef7-60dbe3d946d6.box" });
        List<Page> info = user.queryInfo(new String[] { "File:17d92086-4c10-4ac8-9ef7-60dbe3d946d6.box" });

        System.out.println(images.size());
        System.out.println(contents.size());
        System.out.println(info.size());

        for (Page page : images) {
            System.out.println(page.getImageUrl());
        }
        for (Page content : contents) {
            System.out.println(content.getCurrentContent());
        }
        for (Page content : info) {
            System.out.println(content.getImageUrl());
            System.out.println(content.getCurrentContent());
        }
    }
}
