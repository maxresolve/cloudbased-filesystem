package Config;

import Verbose.Verbose;
import XmlAdapter.XmlValidator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by Kristian on 10-04-2016.
 */

@XmlRootElement(name = "config")
public class Config {
    @XmlElement(name = "root-directory", required = true)
    private String _rootDirectory;
    @XmlElement(name = "local-key", required = true)
    private String _localKey;
    @XmlElement(name = "mediawiki", required = true)
    private MediawikiConfig _mediawikiConfig;


    private static String _XML = "Config.xml";
    private static final String _XSD = System.getProperty("user.dir") + "/src/Config/Config.xsd";

    private Config() {
    }

    public String getRootDirectory() {
        if (this._rootDirectory.endsWith("/")) {
            return this._rootDirectory;
        } else {
            return this._rootDirectory + "/";
        }
    }

    public String getLocalKeyPath() {
        return this._localKey;
    }

    public MediawikiConfig getMediawiki() {
        return this._mediawikiConfig;
    }

    public static Config loadConfig(String xmlPath) {
        _setXML(xmlPath);
        return loadConfig();
    }

    public static Config loadConfig() {
        try {
            File file = new File(_XML);
            if (!file.exists()) {
                throw new FileNotFoundException("Config file is missing, please add the config file (Config.xml).");
            }

            if (!XmlValidator.isXMLValid(_XML, _XSD)) {
                throw new FileNotFoundException("The XML configuration does not match the config.xsd schema file");
            }

            JAXBContext jaxbContext = JAXBContext.newInstance(Config.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Config config = (Config) jaxbUnmarshaller.unmarshal(file);
            Verbose.print("Config loaded from XML");

            File rootDir = new File(config.getRootDirectory());

            if (!rootDir.isDirectory()) {
                if (!rootDir.mkdir()) {
                    throw new IllegalStateException("Root directory is not a directory");
                }
            }

            return config;
        } catch (JAXBException | FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void _setXML(String path) {
        if (path.endsWith("/")) {           // If it is a directory
            _XML = path + _XML;
        } else if (path.endsWith(".xml")) { // If it is a xml file
            _XML = path;
        } else {                            // If it is a directory but without / at the end
            _XML = path + "/" + _XML;
        }
    }
}
