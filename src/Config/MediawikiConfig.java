package Config;

import Mediawiki.japi.Mediawiki;
import Mediawiki.japi.api.Login;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by mads on 4/19/16.
 */
@XmlRootElement(name = "mediawiki")
public class MediawikiConfig {
    @XmlElement(name = "url", required = true)
    public String url;
    @XmlElement(name = "username", required = false)
    public String username;
    @XmlElement(name = "password", required = false)
    public String password;
    @XmlElement(name = "file-extension", required = true)
    public String fileExtension;

    private Mediawiki wiki;

    public MediawikiConfig() {

    }

    public String getUrl() {
        return this.url;
    }
    public String getUsername() {
        return this.username;
    }
    public String getPassword() {
        return this.password;
    }
    public String getFileExtension() {
        if (!this.fileExtension.startsWith(".")) {
            return "." + this.fileExtension;
        } else {
            return this.fileExtension;
        }
    }

    public boolean isAuthRequired() {
        return this.username != null && !this.username.isEmpty() && this.password != null && !this.password.isEmpty();
    }

    public Mediawiki login() {
        try {
            this.wiki = new Mediawiki(this.url);
            if (this.isAuthRequired()) {
                Login login = this.wiki.login(this.username, this.password);
                if (!login.getResult().equals("Success")) {
                    throw new IllegalStateException("Wrong Mediawiki login");
                }
            }

            return this.wiki;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    public void logout() {
        if (this.wiki != null && this.isAuthRequired()) {
            try {
                this.wiki.logout();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
