package Cryptography;

import java.util.Base64;
/**
 * Created by Mads Lundt on 3/26/16.
 */
public class Encoding {

    /**
     * Encodes a byte array into a string by using Base64
     * @param bytes
     * @return
     */
    public static String encode(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

    /**
     * Decodes a string into a byte array by using Base64
     * @param encoding
     * @return
     */
    public static byte[] decode(String encoding) {
        return Base64.getDecoder().decode(encoding);
    }

    public static boolean isEncoded(String text) {
        return text.matches("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$");
    }
}
