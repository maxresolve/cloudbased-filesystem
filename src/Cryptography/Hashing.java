package Cryptography;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Hashing {
    // http://www.mindrot.org/projects/jBCrypt/

    private static final int _SALT_LENGTH = 32;
    private static final String _MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM = "SHA-256";

    private byte[] _passwordHash;
    private byte[] _salt;

    /**
     * Hash a password and salt it with the included salt.
     * @param password
     * @param salt
     */
    public Hashing(String password, String salt) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(this._MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        password += salt;
        messageDigest.update(password.getBytes());
        this._passwordHash = messageDigest.digest();
    }

    /**
     * Hash a password and salt it with a generated salt.
     * @param password
     */
    public Hashing(String password) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(this._MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        this._salt = this._generateSalt();
        password += this._salt;
        try {
            messageDigest.update(password.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this._passwordHash = messageDigest.digest();
    }

    /**
     * Returns the password hash Base64 encoded.
     * @return
     */
    public String getPasswordHash() {
        return Encoding.encode(this._passwordHash);
    }

    /**
     * Generates a random salt.
     * @return
     */
    private byte[] _generateSalt() {
        final SecureRandom r = new SecureRandom();
        byte[] salt = new byte[_SALT_LENGTH];
        r.nextBytes(salt);

        return salt;
    }

    public static String generateSalt() {
        final SecureRandom r = new SecureRandom();
        byte[] salt = new byte[_SALT_LENGTH];
        r.nextBytes(salt);

        return Encoding.encode(salt);
    }

    /**
     * Checks if the password hash is equal an included plain text password.
     * @param password
     * @return
     */
    public boolean isEqual(String password) {
        // This compares base64 encodings. Should it compare the decoded strings?
        return this.getPasswordHash().equals(password);
    }

    public static String generateFileHash(java.io.File file) {
        try {
            boolean canRead = file.canRead();
            boolean canWrite = file.canWrite();
            file.setReadable(true);
            file.setWritable(true);
            Path path = file.toPath();
            byte[] fileData = Files.readAllBytes(path);

            MessageDigest md = MessageDigest.getInstance(_MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM);

            file.setReadable(canRead);
            file.setWritable(canWrite);

            //getting the md5 hash of the file
            return Encoding.encode(md.digest(fileData));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }
    public static String generateFileHash(InputStream stream) {
        try {
            MessageDigest md = MessageDigest.getInstance(_MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM);

            byte[] buffer = new byte[64];
            int len;
            while ((len = stream.read(buffer)) >= 0)
            {
                md.update(buffer, 0, len);
            }

            return Encoding.encode(md.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
