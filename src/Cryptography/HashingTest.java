package Cryptography;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by mads on 4/19/16.
 */
public class HashingTest {

    @Test
    public void getPasswordHash() throws Exception {
        Hashing hash = new Hashing("Test123");
        Assert.assertNotNull(hash.getPasswordHash());
        Assert.assertTrue(hash.getPasswordHash().length() > 0);
    }

    @Test
    public void generateSalt() throws Exception {
        String salt = Hashing.generateSalt();
        Assert.assertNotNull(salt);
        Assert.assertTrue(salt.length() > 0);
    }

    @Test
    public void generateUniqueSalts() throws Exception {
        Assert.assertNotEquals(Hashing.generateSalt(), Hashing.generateSalt());
    }

    @Test
    public void getFileHashFromInputStream() throws Exception {
        InputStream in = new FileInputStream(System.getProperty("user.dir") + "/src/TestFiles/test.txt");
        InputStream in2 = new FileInputStream(System.getProperty("user.dir") + "/src/TestFiles/test.txt");
        InputStream in3 = new FileInputStream(System.getProperty("user.dir") + "/src/TestFiles/anothertest.txt");
        String hash1 = Hashing.generateFileHash(in);
        String hash2 = Hashing.generateFileHash(in2);
        String hash3 = Hashing.generateFileHash(in3);
        Assert.assertEquals(hash1, hash2);
        Assert.assertNotEquals(hash2, hash3);
        Assert.assertNotEquals(hash1, hash3);
    }

    @Test
    public void getFileHashFromFile() throws Exception {
        File file = new File(System.getProperty("user.dir") + "/src/TestFiles/test.txt");
        File file2 = new File(System.getProperty("user.dir") + "/src/TestFiles/anothertest.txt");
        String hash1 = Hashing.generateFileHash(file);
        String hash2 = Hashing.generateFileHash(file);
        String hash3 = Hashing.generateFileHash(file2);
        Assert.assertEquals(hash1, hash2);
        Assert.assertNotEquals(hash2, hash3);
        Assert.assertNotEquals(hash1, hash3);
    }

    @Test
    public void isEqual() throws Exception {
        String salt = Hashing.generateSalt();
        Hashing hash1 = new Hashing("Test123", salt);

        String correctPasswordHash = new Hashing("Test123", salt).getPasswordHash();
        String wrongPasswordHash1 = new Hashing("wrong123", salt).getPasswordHash();
        String wrongPasswordHash2 = new Hashing("Test123").getPasswordHash(); // Different salt

        Assert.assertTrue(hash1.isEqual(correctPasswordHash));
        Assert.assertFalse(hash1.isEqual(wrongPasswordHash1));
        Assert.assertFalse(hash1.isEqual(wrongPasswordHash2));
    }
}