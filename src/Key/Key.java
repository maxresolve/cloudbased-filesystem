package Key;

import Config.MediawikiConfig;
import Cryptography.Asymmetric;
import Cryptography.Encoding;
import Cryptography.Hashing;
import Cryptography.Symmetric;
import Mediawiki.japi.Mediawiki;
import Mediawiki.japi.api.Revision;
import XmlAdapter.DateAdapter;
import XmlAdapter.XmlValidator;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@XmlRootElement(name = "key")
public class Key {
    @XmlAttribute(name = "id", required = true)
    protected String _id;
    @XmlElement(name = "symmetric-key", required = true)
    protected String _symmetricKey;
    @XmlElement(name = "iv", required = true)
    protected String _iv;
    @XmlElement(name = "name", required = false)
    protected String _name = null;
    @XmlAttribute(name = "type", required = true)
    protected KeyType _type = KeyType.FileKey;
    @XmlElement(name = "read-key", required = false)
    protected String _readKey = null;
    @XmlElement(name = "write-key", required = false)
    protected String _writeKey = null;
    @XmlElement(name = "reference", required = true)
    protected String _reference;
    @XmlElement(name = "mediawiki", required = true)
    protected MediawikiConfig _mediaWiki;
    @XmlElement(name = "added", required = true)
    @XmlJavaTypeAdapter(DateAdapter.class)
    protected Date _added;

    private KeyRing _keyRing = null;

    private static final String _XSD = System.getProperty("user.dir") + "/src/Key/Key.xsd";

    private Key() {

    }

    public Key(KeyRing keyRing) {
        this._type = KeyType.KeyRing;
        this._mediaWiki = Config.Config.loadConfig().getMediawiki();
        this._reference = keyRing.getId();
        this._id = UUID.randomUUID().toString();
        this._symmetricKey = Symmetric.generateKey();
        this._iv = Symmetric.generateIv();
        Asymmetric asym = new Asymmetric();
        this._readKey = asym.getPublicKey();
        this._writeKey = asym.getPrivateKey();
        this._added = new Date();
        this._keyRing = keyRing;
    }
    public Key(KeyRing keyRing, MediawikiConfig wiki) {
        this(keyRing);
        this._mediaWiki = wiki;
    }
    public Key(File file) {
        this._id = UUID.randomUUID().toString();
        this._reference = UUID.randomUUID().toString();
        this._mediaWiki = Config.Config.loadConfig().getMediawiki();
        this._name = file.getName();

        Asymmetric asym = new Asymmetric();
        this._readKey = asym.getPublicKey();
        this._writeKey = asym.getPrivateKey();
        this._symmetricKey = Symmetric.generateKey();
        this._iv = Symmetric.generateIv();
        this._added = new Date();
    }
    public Key(File file, MediawikiConfig wiki) {
        this(file);
        this._mediaWiki = wiki;
    }

    public String getId() {
        return this._id;
    }
    public String getReference() {
        return this._reference;
    }
    public KeyType getType() {
        return this._type;
    }
    @XmlTransient
    public String getName() {
        if (this._name != null && !this._name.isEmpty()) {
            return this._name;
        } else {
            return this._reference;
        }
    }
    public KeyRing getReferenceKeyRing() {
        return this._keyRing;
    }
    public String getWriteKey() {
        return this._writeKey;
    }
    public String getReadKey() {
        return this._readKey;
    }

    public void setName(String name) {
        this._name = name;
    }

    public boolean encryptAndUpload(File decryptedFile) {
        File encryptedFile = this._encryptFile(decryptedFile);
        if (encryptedFile == null) {
            throw new IllegalStateException();
        }
        if (this._writeKey != null && !this._writeKey.isEmpty()) {
            return this._uploadFile(encryptedFile);
        }

        return false;
    }
    public boolean encryptAndUpload(KeyRing keyRing) {
        File file = new File(this._reference + ".xml");
        keyRing.saveXML(file);
        boolean success = this.encryptAndUpload(file);

        file.delete();

        return success;
    }

    private boolean _uploadFile(File encryptedFile) {
        boolean success = false;
        try {
            Mediawiki wiki = this._mediaWiki.login();
            InputStream in = new FileInputStream(encryptedFile);
            String fileName = this._reference + this._mediaWiki.getFileExtension();

            String encryptedFileHash = this._generateEncryptedFileHash(in);
            if (encryptedFileHash == null || encryptedFileHash.isEmpty()) {
                in.close();
                throw new IllegalStateException("Could not encrypt file hash");
            } else {
                String content = wiki.getPageContent("File:" + fileName);
                if (content == null || !content.equals(encryptedFileHash)) {
                    in = new FileInputStream(encryptedFile);
                    try {
                        wiki.upload(in, fileName, encryptedFileHash, "");
                        wiki.edit("File:" + fileName, encryptedFileHash, ""); // Have to edit page because upload does not change content but only adds.
                        success = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new IllegalStateException();
                    }
                    in.close();
                } else {
                    System.out.println("File is no different from the file uploaded earlier. Upload has been cancelled.");
                }

                encryptedFile.delete();
            }

            this._mediaWiki.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }

    private File _encryptFile(File decryptedFile) {
        try {
            File encryptedFile = new File(this._reference + this._mediaWiki.getFileExtension());
            Symmetric sym = new Symmetric(this._symmetricKey, this._iv);
            InputStream fileKeyInput = new FileInputStream(decryptedFile);
            OutputStream fileKeyOutput = new FileOutputStream(encryptedFile);
            sym.encrypt(fileKeyInput, fileKeyOutput);
            fileKeyInput.close();
            fileKeyOutput.close();

            return encryptedFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public File decrypt(File encryptedFile, File decryptedFile, boolean deleteEncryptedFile) {
        if (!decryptedFile.exists()) {
            try {
                decryptedFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        try {
            Symmetric sym = new Symmetric(this._symmetricKey, this._iv);
            InputStream fileKeyInput = new FileInputStream(encryptedFile);
            OutputStream fileKeyOutput = new FileOutputStream(decryptedFile);
            sym.decrypt(fileKeyInput, fileKeyOutput);
            fileKeyInput.close();
            fileKeyOutput.close();
            if (deleteEncryptedFile) {
                encryptedFile.delete();
            }

            return decryptedFile;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String _generateEncryptedFileHash(InputStream in) {
        if (this._writeKey != null && !this._writeKey.isEmpty()) {
            String fileHash = Hashing.generateFileHash(in);
            Asymmetric asym = new Asymmetric(null, this._writeKey);

            try {
                String encryptedFileHash = asym.encryptByUsingPrivateKey(fileHash);
                return encryptedFileHash;
            } catch (InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public File download(String path, boolean runThroughRevisions) {
        if (!path.endsWith("/")) {
            path += "/";
        }
        Mediawiki wiki = this._mediaWiki.login();
        String pageName = "File:" + this._reference + this._mediaWiki.getFileExtension();
        String url = null;
        String content = null;
        try {
            url = wiki.getImageInfo(pageName).getUrl();
            content = wiki.getPageContent(pageName);
        } catch (Exception e) {
            e.printStackTrace();
            this._mediaWiki.logout();
        }
        File download = this._downloadFile(url, content, path);
        if (download == null) {
            if (runThroughRevisions) {
                try {
                    download = this._findCorrectFile(wiki.getRevisions(pageName), path);
                } catch (Exception e) {
                    e.printStackTrace();
                    this._mediaWiki.logout();
                    throw new IllegalStateException("File does not match file content");
                }
            } else {
                throw new IllegalStateException("File does not match file content");
            }
        }

        this._mediaWiki.logout();

        return download;
    }

    public Key exportKey(boolean readAccess, boolean writeAccess) {
        Key key = new Key();
        key._symmetricKey = this._symmetricKey;
        key._iv = this._iv;
        key._id = this._id;
        key._reference = this._reference;
        key._mediaWiki = this._mediaWiki;
        key._name = this._name;

        if (readAccess) {
            key._readKey = this._readKey;
        }
        if (writeAccess) {
            key._writeKey = this._writeKey;
        }
        key._added = new Date();

        return key;
    }

    public void saveXML(File file) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Key.class);
            Marshaller marshaller = jc.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, _XSD);

            if (file == null || !file.exists() || !file.isFile()) {
                file.createNewFile();
            }
            OutputStream out = new FileOutputStream(file);
            marshaller.marshal(this, out);
            out.close();
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }

    public static Key loadKeyFromXML(File file, String privateKey, String encryptionKey) {
        Asymmetric asym = new Asymmetric(null, privateKey);
        try {
            String decryptedKey = new String(Encoding.decode(asym.decryptByUsingPrivateKey(encryptionKey)), Charset.forName("UTF-8"));
            String[] keys = decryptedKey.split(" : ");
            String keyStr = keys[0];
            String ivStr = keys[1];

            File decryptedFile = new File("tempKey.xml");

            Symmetric sym = new Symmetric(keyStr, ivStr);
            InputStream in = new FileInputStream(file);
            OutputStream out = new FileOutputStream(decryptedFile);
            sym.decrypt(in, out);
            in.close();
            out.close();

            Key key = loadKeyFromXML(decryptedFile);
            decryptedFile.delete();
            if (key != null) {
                return key;
            } else {
                throw new IllegalStateException("Key could not be loaded");
            }
        } catch (InvalidKeySpecException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new IllegalStateException("Wrong private key");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Key loadKeyFromXML(File file) {
        try {
            if (file == null || !file.exists() || !file.isFile()) {
                return null;
            }

            if (!XmlValidator.isXMLValid(file.getPath(), _XSD)) {
                return null;
                //throw new FileNotFoundException("The XML configuration does not match the Key.xsd schema file");
            }

            JAXBContext jaxbContext = JAXBContext.newInstance(Key.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Key key = (Key) jaxbUnmarshaller.unmarshal(file);

            return key;
        } catch (JAXBException e) {
            try {
                return _parseXML(file);
            } catch (Exception e1) {
                e.printStackTrace();
                //e1.printStackTrace();
            }
        }
        return null;
    }

    private static Key _parseXML(File file) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource e = new InputSource(new FileReader(file));
        Document doc = db.parse(e);
        XPath xPath = XPathFactory.newInstance().newXPath();
        XPathExpression expr = xPath.compile("key");
        NodeList nl = (NodeList)expr.evaluate(doc, XPathConstants.NODESET);
        Node node = nl.item(0);

        return parseNode(node);
    }

    public static Key parseNode(Node node) throws Exception {
        Key key = new Key();
        key._id = _tryGetContent(node.getAttributes().getNamedItem("id"));
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node n = children.item(i);
            if (n.getNodeName().equalsIgnoreCase("iv")) {
                key._iv = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("added")) {
                DateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
                key._added = format.parse(_tryGetContent(n));
            } else if (n.getNodeName().equalsIgnoreCase("symmetric-key")) {
                key._symmetricKey = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("iv")) {
                key._iv = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("iv")) {
                key._iv = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("read-key")) {
                key._readKey = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("write-key")) {
                key._writeKey = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("reference")) {
                key._reference = _tryGetContent(n);
            } else if (n.getNodeName().equalsIgnoreCase("type")) {
                key._type = KeyType.values()[Integer.parseInt(_tryGetContent(n))];
            } else if (n.getNodeName().equalsIgnoreCase("mediawiki")) {
                NodeList mediawikiChildren = n.getChildNodes();
                for (int j = 0; j < mediawikiChildren.getLength(); j++) {
                    Node mediawikiNode = mediawikiChildren.item(j);
                    MediawikiConfig wiki = new MediawikiConfig();
                    if (mediawikiNode.getNodeName().equalsIgnoreCase("url")) {
                        wiki.url = _tryGetContent(mediawikiNode);
                    } else if (mediawikiNode.getNodeName().equalsIgnoreCase("username")) {
                        wiki.username = _tryGetContent(mediawikiNode);
                    } else if (mediawikiNode.getNodeName().equalsIgnoreCase("password")) {
                        wiki.password = _tryGetContent(mediawikiNode);
                    } else if (mediawikiNode.getNodeName().equalsIgnoreCase("file-extension")) {
                        wiki.fileExtension = _tryGetContent(mediawikiNode);
                    } else {
                        continue;
                    }
                }
            } else if (n.getNodeName().equalsIgnoreCase("name")) {
                key._name = _tryGetContent(n);
            } else {
                continue;
            }
        }

        return key;
    }

    private File _downloadFile(String url, String content, String path) {
        File file = new File(path + this.getReference() + this._mediaWiki.getFileExtension());
        try {
            InputStream in = new URL(url).openStream();
            if (content == null || content.isEmpty() || !this._validateFile(in, content)) {
                in.close();
                return null;
            } else {
                in = new URL(url).openStream();
                OutputStream out = new FileOutputStream(file);
                IOUtils.copy(in, out);
                out.close();
            }
            in.close();

            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
    private File _downloadFile(String url, String path) {
        File file = new File(path + this.getReference() + this._mediaWiki.getFileExtension());
        try {
            InputStream in = new URL(url).openStream();
            OutputStream out = new FileOutputStream(file);
            IOUtils.copy(in, out);
            out.close();
            in.close();

            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private File _findCorrectFile(List<Revision> revisions, String path) {
        for (Revision revision : revisions) {
            File rev = this._downloadFile(revision.getUrl(), revision.getContent(), path);
            if (rev != null) {
                return rev;
            }
        }

        return null;
    }

    private boolean _validateFile(InputStream in, String encryptedFileHash) {
        if (this._readKey != null && !this._readKey.isEmpty()) {
            String fileHash = Hashing.generateFileHash(in);
            Asymmetric asym = new Asymmetric(this._readKey);

            try {
                String decryptedFileHash = asym.decryptByUsingPublicKey(encryptedFileHash);
                return fileHash.equals(decryptedFileHash);
            } catch (InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {}
        }

        return false;
    }

    private static String _tryGetContent(Node n) {
        if (n != null && n.getTextContent() != null) {
            return n.getTextContent();
        } else {
            return null;
        }
    }
}
