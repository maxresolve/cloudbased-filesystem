package Key;

import Cryptography.Symmetric;
import XmlAdapter.DateAdapter;
import XmlAdapter.KeyAdapter;
import XmlAdapter.XmlValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by mads on 5/13/16.
 */
@XmlRootElement(name = "key-ring")
public class KeyRing {
    @XmlAttribute(name = "id", required = true)
    private String _id;
    @XmlElement(name = "keys", required = true)
    @XmlJavaTypeAdapter(KeyAdapter.class)
    private HashMap<String, Key> _keys = new HashMap<>();
    @XmlElement(name = "last-updated")
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date _lastUpdated = new Date();

    private static final String _XSD = System.getProperty("user.dir") + "/src/Key/KeyRing.xsd";

    private KeyRing() {

    }

    public KeyRing(HashMap<String, Key> keys) {
        this._id = UUID.randomUUID().toString();
        if (keys.size() > 0) {
            this._keys = keys;
        }
    }

    public KeyRing(List<Key> keys) {
        this._id = UUID.randomUUID().toString();
        if (keys.size() > 0) {
            this.addKey(keys);
        }
    }

    public String getId() {
        return this._id;
    }

    public HashMap<String, Key> getKeys() {
        return this._keys;
    }

    public void addKey(Key key, boolean replace) {
        this._addKey(key, replace);
    }
    public void addKey(Key key) {
        this.addKey(key, false);
    }

    private void _addKey(Key key, boolean replace) {
        if (!this._keys.containsKey(key.getId().toLowerCase())) {
            this._lastUpdated = new Date();
            this._keys.put(key.getId(), key);
        } else if (replace) {
            this._keys.replace(key.getId(), key);
        }
    }

    public void addKey(List<Key> keys) {
        if (keys.size() > 0) {
            for (Key key : keys) {
                this._addKey(key, false);
            }
        }
    }
    public void addKey(HashMap<String, Key> keys) {
        if (keys.size() > 0) {
            for (Map.Entry<String, Key> entry : keys.entrySet()) {
                this._addKey(entry.getValue(), false);
            }
        }
    }
    public void addEncryptedKey(File file, String privateKey, String encryptionKey, boolean replace) {
        Key key = Key.loadKeyFromXML(file, privateKey, encryptionKey);
        if (key != null) {
            this._addKey(key, replace);
        }
    }
    public void addEncryptedKey(File file, String privateKey, String encryptionKey) {
        this.addEncryptedKey(file, privateKey, encryptionKey, false);
    }

    public void removeKey(String id) {
        this._removeKey(id);
    }

    public Key getKey(String id) {
        if (this._keys.containsKey(id)) {
            return this._keys.get(id);
        } else {
            for (Entry<String, Key> entry : this._keys.entrySet()) {
                Key key = entry.getValue();
                if (key.getType() == KeyType.KeyRing) {
                    if (key.getReferenceKeyRing() != null) {
                        key.getReferenceKeyRing().getKey(id);
                    } else {
                        File encryptedFile = key.download(".", true);
                        File decryptedFile = new File(key.getReference() + ".xml");
                        key.decrypt(encryptedFile, decryptedFile, true);
                        KeyRing keyRing = KeyRing.loadXML(decryptedFile.getPath());
                        decryptedFile.delete();
                        keyRing.getKey(id);
                    }
                }
            }
        }

        return null;
    }

    public Key getKeyReferingToKeyRing(String id) {
        for (Entry<String, Key> entry : this._keys.entrySet()) {
            Key key = entry.getValue();
            if (key.getReference().equals(id)) {
                return key;
            }
            if (key.getType() == KeyType.KeyRing) {
                if (key.getReferenceKeyRing() != null) {
                    key.getReferenceKeyRing().getKeyReferingToKeyRing(id);
                } else {
                    File encryptedFile = key.download(".", true);
                    File decryptedFile = new File(key.getReference() + ".xml");
                    key.decrypt(encryptedFile, decryptedFile, true);
                    KeyRing keyRing = KeyRing.loadXML(decryptedFile.getPath());
                    decryptedFile.delete();
                    keyRing.getKeyReferingToKeyRing(id);
                }
            }
        }

        return null;
    }

    public void overwriteKey(Key key) {
        this.removeKey(key.getId());
        this.addKey(key);
    }

    public Key getKeyByFilename(String filename) {
        for (Entry<String, Key> entry : this._keys.entrySet()) {
            Key key = entry.getValue();
            if (key.getType() == KeyType.KeyRing) {
                if (key.getReferenceKeyRing() != null) {
                    key.getReferenceKeyRing().getKeyByFilename(filename);
                } else {
                    File encryptedFile = key.download(".", true);
                    File decryptedFile = new File(key.getReference() + ".xml");
                    key.decrypt(encryptedFile, decryptedFile, true);
                    KeyRing keyRing = KeyRing.loadXML(decryptedFile.getPath());
                    decryptedFile.delete();
                    keyRing.getKeyByFilename(filename);
                }
            } else if (key.getName().equals(filename)) {
                return key;
            }
        }

        return null;
    }

    public KeyRing getKeyRingContainingKey(String id) {
        if (this._keys.containsKey(id)) {
            return this;
        } else {
            for (Entry<String, Key> entry : this._keys.entrySet()) {
                Key key = entry.getValue();
                if (key.getType() == KeyType.KeyRing) {
                    if (key.getReferenceKeyRing() != null) {
                        key.getReferenceKeyRing().getKey(id);
                    } else {
                        File encryptedFile = key.download(".", true);
                        File decryptedFile = new File(key.getReference() + ".xml");
                        key.decrypt(encryptedFile, decryptedFile, true);
                        KeyRing keyRing = KeyRing.loadXML(decryptedFile.getPath());
                        decryptedFile.delete();
                        keyRing.getKey(id);
                    }
                }
            }
        }

        return null;
    }

    public KeyRing getKeyRing(String id) {
        if (this._id.equals(id)) {
            return this;
        } else {
            for (Entry<String, Key> entry : this._keys.entrySet()) {
                Key key = entry.getValue();
                if (key.getType() == KeyType.KeyRing) {
                    if (key.getReferenceKeyRing() != null) {
                        key.getReferenceKeyRing().getKey(id);
                    }
                }
            }
        }

        return null;
    }

    public void downloadAll(String path, boolean runRecursive) {
        if (!path.endsWith("/")) {
            path += "/";
        }
        File dir = new File(path);
        if (!dir.isDirectory()) {
            dir.mkdir();
        }
        for (Entry<String, Key> entry : this._keys.entrySet()) {
            Key key = entry.getValue();
            if (key.getType() == KeyType.FileKey) {
                File encryptedFile = key.download(path, true);
                File decryptedFile = new File(path + key.getName());
                if (!decryptedFile.exists() || !decryptedFile.isFile()) {
                    try {
                        decryptedFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                key.decrypt(encryptedFile, decryptedFile, true);
            } else if (key.getType() == KeyType.KeyRing) {
                File encryptedFile = key.download(path, true);
                File decryptedFile = new File(path + key.getReference() + ".xml");
                key.decrypt(encryptedFile, decryptedFile, true);
                KeyRing keyRing = KeyRing.loadXML(decryptedFile.getPath());
                if (runRecursive) {
                    keyRing.downloadAll(path, true);
                }
            }
        }
    }

    public static KeyRing decrypt(String id, File encryptedFile, String symmetricKey, String iv, boolean deleteEncryptedFile) {
        try {
            File decryptedFile = new File(id + ".xml");
            Symmetric sym = new Symmetric(symmetricKey, iv);
            InputStream fileKeyInput = new FileInputStream(encryptedFile);
            OutputStream fileKeyOutput = new FileOutputStream(decryptedFile);
            sym.decrypt(fileKeyInput, fileKeyOutput);
            fileKeyInput.close();
            fileKeyOutput.close();
            if (deleteEncryptedFile) {
                encryptedFile.delete();
            }
            return loadXML(decryptedFile.getPath());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private void _removeKey(String id) {
        if (this._keys.containsKey(id)) {
            this._lastUpdated = new Date();
            this._keys.remove(id);
        }
    }

    private void _saveXML() {
        this._lastUpdated = new Date();
        try {
            JAXBContext jc = JAXBContext.newInstance(KeyRing.class);
            Marshaller marshaller = jc.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, _XSD);

            File file = new File(this._id + ".xml");
            if (file == null || !file.exists() || !file.isFile()) {
                file.createNewFile();
            }
            OutputStream out = new FileOutputStream(file);
            marshaller.marshal(this, out);
            out.close();
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }

    public void saveXML(File file) {
        try {
            JAXBContext jc = JAXBContext.newInstance(KeyRing.class);
            Marshaller marshaller = jc.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, _XSD);

            if (file == null || !file.exists() || !file.isFile()) {
                file.createNewFile();
            }
            OutputStream out = new FileOutputStream(file);
            marshaller.marshal(this, out);
            out.close();
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }

    public static KeyRing loadXML(String xmlPath) {
        File file = null;
        try {
            file = new File(xmlPath);

            if (file == null || !file.exists() || !file.isFile()) {
                throw new FileNotFoundException();
            }

            if (!XmlValidator.isXMLValid(xmlPath, _XSD)) {
                return null;
                //throw new IllegalStateException("The XML configuration does not match the KeyRing.xsd schema file");
            }

            JAXBContext jaxbContext = JAXBContext.newInstance(KeyRing.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            KeyRing keys = (KeyRing) jaxbUnmarshaller.unmarshal(file);

            return keys;
        } catch (JAXBException e) {
            try {
                return _parseXML(file);
            } catch (Exception e2) {
                e.printStackTrace();
                e2.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static KeyRing _parseXML(File file) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource e = new InputSource(new FileReader(file));
        Document doc = db.parse(e);
        XPath xPath = XPathFactory.newInstance().newXPath();
        XPathExpression expr = xPath.compile("key-ring");
        NodeList nl = (NodeList)expr.evaluate(doc, XPathConstants.NODESET);
        KeyRing keyRing = new KeyRing();
        Node node = nl.item(0);
        keyRing._id = _tryGetContent(node.getAttributes().getNamedItem("id"));

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node n = children.item(i);
            if (n.getNodeName().equalsIgnoreCase("keys")) {
                NodeList keysChildren = n.getChildNodes();
                for (int j = 0; j < keysChildren.getLength(); j++) {
                    Key key = Key.parseNode(keysChildren.item(j));
                    keyRing._keys.put(key.getId(), key);
                }
            } else if (n.getNodeName().equalsIgnoreCase("last-updated")) {
                DateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
                keyRing._lastUpdated = format.parse(_tryGetContent(n));
            } else {
                continue;
            }
        }
        return keyRing;
    }

    private static String _tryGetContent(Node n) {
        if (n != null && n.getTextContent() != null) {
            return n.getTextContent();
        } else {
            return null;
        }
    }
}
