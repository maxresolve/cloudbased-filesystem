package Key;

import Config.Config;
import Cryptography.Asymmetric;
import Cryptography.Hashing;
import Cryptography.Symmetric;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by mads on 5/14/16.
 */
public class KeyTest {
    private File _file;
    private Config _config;

    @Before
    public void setUp() throws Exception {
        this._file = new File(System.getProperty("user.dir") + "/src/TestFiles/test.txt");
        this._config = Config.loadConfig();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void initKeyKeyRing() {
        KeyRing keyRing = new KeyRing(new ArrayList<Key>());
        Key key = new Key(keyRing);

        Assert.assertNotNull(key.getId());
        Assert.assertFalse(key.getId().isEmpty());
        Assert.assertEquals(key.getReference(), keyRing.getId());
        Assert.assertEquals(key.getType(), KeyType.KeyRing);

        new File(keyRing.getId() + ".xml").delete();
    }
    @Test
    public void initKeyFile() {
        Key key = new Key(this._file);
        Assert.assertNotNull(key.getId());
        Assert.assertFalse(key.getId().isEmpty());
        Assert.assertNotNull(key.getReference());
        Assert.assertFalse(key.getReference().isEmpty());
        Assert.assertEquals(key.getType(), KeyType.FileKey);
    }
    @Test
    public void encryptFile() {
        Key key = new Key(this._file);
        key.encryptAndUpload(this._file);

        File encryptedFile = key.download(System.getProperty("user.dir"), false);
        Assert.assertTrue(encryptedFile.exists());

        Assert.assertNotEquals(Hashing.generateFileHash(this._file), Hashing.generateFileHash(encryptedFile));

        encryptedFile.delete();
    }
    @Test
    public void decryptFile() {
        Key key = new Key(this._file);
        key.encryptAndUpload(this._file);

        File decryptedFile = new File(System.getProperty("user.dir") + "/" + key.getName());
        Assert.assertFalse(decryptedFile.exists());

        File encryptedFile = key.download(System.getProperty("user.dir"), false);
        Assert.assertTrue(encryptedFile.exists());

        key.decrypt(encryptedFile, decryptedFile, false);
        Assert.assertTrue(encryptedFile.exists());
        decryptedFile.delete();

        key.decrypt(encryptedFile, decryptedFile, true);
        Assert.assertFalse(encryptedFile.exists());
        Assert.assertTrue(decryptedFile.exists());
        Assert.assertEquals(Hashing.generateFileHash(this._file), Hashing.generateFileHash(decryptedFile));

        decryptedFile.delete();
        encryptedFile.delete();
    }
    @Test
    public void encryptKeyRing() {
        KeyRing tempKeyRing = new KeyRing(new ArrayList<Key>());
        Key key = new Key(tempKeyRing);
        key.encryptAndUpload(tempKeyRing);

        File encryptedFile = key.download(System.getProperty("user.dir"), false);
        Assert.assertTrue(encryptedFile.exists());
        Assert.assertNotEquals(Hashing.generateFileHash(this._file), Hashing.generateFileHash(encryptedFile));

        encryptedFile.delete();
    }
    @Test
    public void decryptKeyRing() {
        KeyRing tempKeyRing = new KeyRing(new ArrayList<Key>());
        Key key = new Key(tempKeyRing);
        key.encryptAndUpload(tempKeyRing);

        File decryptedFile = new File(System.getProperty("user.dir") + "/" + key.getReference() + "2.xml");
        Assert.assertFalse(decryptedFile.exists());

        File encryptedFile = key.download(System.getProperty("user.dir"), false);
        Assert.assertTrue(encryptedFile.exists());

        key.decrypt(encryptedFile, decryptedFile, false);
        Assert.assertTrue(encryptedFile.exists());
        decryptedFile.delete();

        key.decrypt(encryptedFile, decryptedFile, true);
        Assert.assertFalse(encryptedFile.exists());
        Assert.assertTrue(decryptedFile.exists());

        KeyRing keyRing = KeyRing.loadXML(decryptedFile.getPath());
        Assert.assertEquals(key.getReference(), keyRing.getId());

        decryptedFile.delete();
        encryptedFile.delete();
    }
    @Test
    public void exportKeyOnlyWithReadPermission() {
        File file = new File(System.getProperty("user.dir") + "/src/TestFiles/newkey.box");
        if (!file.exists() || !file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Key key = new Key(_file);

        Assert.assertNotNull(key.getReadKey());
        Assert.assertNotNull(key.getWriteKey());

        Key newKey = key.exportKey(true, false);

        Assert.assertNotNull(newKey.getReadKey());
        Assert.assertNull(newKey.getWriteKey());

        Asymmetric asym = new Asymmetric();
        String secret = Symmetric.generateKey();
        String iv = Symmetric.generateIv();
        Symmetric sym = new Symmetric(secret, iv);
        File decryptedFile = new File(newKey.getId());
        newKey.saveXML(decryptedFile);

        String encryptedSecret;

        try {
            InputStream in = new FileInputStream(decryptedFile);
            OutputStream out = new FileOutputStream(file);
            sym.encrypt(in, out);

            Assert.assertNotEquals(Hashing.generateFileHash(decryptedFile), Hashing.generateFileHash(file));

            decryptedFile.delete();

            encryptedSecret = asym.encryptByUsingPublicKey(secret + " : " + iv);
            Assert.assertNotEquals(encryptedSecret, secret + " : " + iv);
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }

        file.delete();
    }
    @Test
    public void exportKeyOnlyWithAllPermissions() {
        File file = new File(System.getProperty("user.dir") + "/src/TestFiles/newkey.box");
        if (!file.exists() || !file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Key key = new Key(_file);

        Assert.assertNotNull(key.getReadKey());
        Assert.assertNotNull(key.getWriteKey());

        Key newKey = key.exportKey(true, true);

        Assert.assertNotNull(newKey.getReadKey());
        Assert.assertNotNull(newKey.getWriteKey());

        Asymmetric asym = new Asymmetric();
        String secret = Symmetric.generateKey();
        String iv = Symmetric.generateIv();
        Symmetric sym = new Symmetric(secret, iv);
        File decryptedFile = new File(newKey.getId());
        newKey.saveXML(decryptedFile);

        String encryptedSecret;

        try {
            InputStream in = new FileInputStream(decryptedFile);
            OutputStream out = new FileOutputStream(file);
            sym.encrypt(in, out);

            Assert.assertNotEquals(Hashing.generateFileHash(decryptedFile), Hashing.generateFileHash(file));

            decryptedFile.delete();

            encryptedSecret = asym.encryptByUsingPublicKey(secret + " : " + iv);
            Assert.assertNotEquals(encryptedSecret, secret + " : " + iv);
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }

        file.delete();
    }
    @Test
    public void addEncryptedKeyOnlyWithReadPermission() {
        File file = new File(System.getProperty("user.dir") + "/src/TestFiles/newkey.box");
        if (!file.exists() || !file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Key key = new Key(_file);

        Assert.assertNotNull(key.getReadKey());
        Assert.assertNotNull(key.getWriteKey());

        Key newKey = key.exportKey(true, false);

        Assert.assertNotNull(newKey.getReadKey());
        Assert.assertNull(newKey.getWriteKey());

        Asymmetric asym = new Asymmetric();
        String secret = Symmetric.generateKey();
        String iv = Symmetric.generateIv();
        Symmetric sym = new Symmetric(secret, iv);
        File decryptedFile = new File(newKey.getId());
        newKey.saveXML(decryptedFile);

        String encryptedSecret;

        try {
            InputStream in = new FileInputStream(decryptedFile);
            OutputStream out = new FileOutputStream(file);
            sym.encrypt(in, out);

            Assert.assertNotEquals(Hashing.generateFileHash(decryptedFile), Hashing.generateFileHash(file));

            decryptedFile.delete();

            encryptedSecret = asym.encryptByUsingPublicKey(secret + " : " + iv);
            Assert.assertNotEquals(encryptedSecret, secret + " : " + iv);

            Key decryptedKey = Key.loadKeyFromXML(file, asym.getPrivateKey(), encryptedSecret);
            Assert.assertEquals(decryptedKey.getId(), key.getId());
            Assert.assertNotNull(decryptedKey.getReadKey());
            Assert.assertNull(decryptedKey.getWriteKey());
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }

        file.delete();
    }
    @Test
    public void addEncryptedKeyWithAllPermissions() {
        File file = new File(System.getProperty("user.dir") + "/src/TestFiles/newkey.box");
        if (!file.exists() || !file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Key key = new Key(_file);

        Assert.assertNotNull(key.getReadKey());
        Assert.assertNotNull(key.getWriteKey());

        Key newKey = key.exportKey(true, true);

        Assert.assertNotNull(newKey.getReadKey());
        Assert.assertNotNull(newKey.getWriteKey());

        Asymmetric asym = new Asymmetric();
        String secret = Symmetric.generateKey();
        String iv = Symmetric.generateIv();
        Symmetric sym = new Symmetric(secret, iv);
        File decryptedFile = new File(newKey.getId());
        newKey.saveXML(decryptedFile);

        String encryptedSecret;

        try {
            InputStream in = new FileInputStream(decryptedFile);
            OutputStream out = new FileOutputStream(file);
            sym.encrypt(in, out);

            Assert.assertNotEquals(Hashing.generateFileHash(decryptedFile), Hashing.generateFileHash(file));

            decryptedFile.delete();

            encryptedSecret = asym.encryptByUsingPublicKey(secret + " : " + iv);
            Assert.assertNotEquals(encryptedSecret, secret + " : " + iv);

            Key decryptedKey = Key.loadKeyFromXML(file, asym.getPrivateKey(), encryptedSecret);
            Assert.assertEquals(decryptedKey.getId(), key.getId());
            Assert.assertNotNull(decryptedKey.getReadKey());
            Assert.assertNotNull(decryptedKey.getWriteKey());
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }

        file.delete();
    }
}