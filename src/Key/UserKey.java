package Key;

import Cryptography.Symmetric;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Created by mads on 5/13/16.
 */
@XmlRootElement(name = "user-key")
public class UserKey {
    @XmlElement(name = "username")
    private String _username;
    @XmlElement(name = "reference")
    private String _reference;
    @XmlElement(name = "symmetric-key")
    private String _symmetricKey;
    @XmlElement(name = "iv")
    private String _iv;

    private String _salt;

    private File file;

    private String _XML;
    private static final String _XSD = System.getProperty("user.dir") + "/src/Key/UserKey.xsd";

    private UserKey() {

    }

    public UserKey(String username, String reference) {
        this._username = username;
        this._reference = reference;
        this._iv = Symmetric.generateIv();
        this._symmetricKey = Symmetric.generateKey();
    }

    public void encrypt(String password) {
        /*try {
            Symmetric sym = new Symmetric(this._symmetricKey, this._salt);
            InputStream fileKeyInput = new FileInputStream(this._id + ".xml");
            OutputStream fileKeyOutput = new FileOutputStream(this.getEncryptedFilename());
            sym.encrypt(fileKeyInput, fileKeyOutput);
            this._file = new File(this.getEncryptedFilename());
            fileKeyInput.close();
            fileKeyOutput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /*public static UserKey getUserKey(String username, String password) {

    }*/
}
